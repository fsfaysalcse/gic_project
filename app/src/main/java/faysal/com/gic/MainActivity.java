package faysal.com.gic;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.PatternMatcher;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    
    EditText getEmail;
    EditText getPassword;
    Button logInBtn;
    TextView forgotPassword;
    TextView newAccount;
    Typeface typeface;
    Typeface typeface2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeface=Typeface.createFromAsset(getAssets(),"fonts/sang.ttf");
        typeface2=Typeface.createFromAsset(getAssets(),"fonts/Tekton.otf");

        getEmail=(EditText)findViewById(R.id.email);
        getPassword=(EditText)findViewById(R.id.password);
        forgotPassword=(TextView)findViewById(R.id.forgotBtn);
        newAccount=(TextView)findViewById(R.id.createAccountBtn);
        logInBtn=(Button)findViewById(R.id.logInBtn);

        getEmail.setTypeface(typeface);
        getPassword.setTypeface(typeface);

        logInBtn.setTypeface(typeface2);
        newAccount.setTypeface(typeface2);

        newAccount.setOnClickListener(this);
        
        logInBtn.setOnClickListener(this);

        
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.logInBtn:
                logInOperation();
                break;

            case R.id.createAccountBtn:
                createAccount();
                break;



        }
    }

    private void createAccount(){
        startActivity(new Intent(getApplicationContext(),SignUp.class));
    }

    private void logInOperation(){
        String email=getEmail.getText().toString().trim();
        String passwoord=getEmail.getText().toString().trim();

        if (email.isEmpty()){
            getEmail.setError("Email is requred");
            getEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            getEmail.setError("Enter valid email");
            getEmail.requestFocus();
            return;
        }

        if (getPassword.getText().toString().isEmpty()){
            getPassword.setError("Password is requred");
            getPassword.requestFocus();
            return;
        }

        if (getPassword.getText().toString().length() <6){
            getPassword.setError("password should be 6 cherector long");
            getPassword.requestFocus();
            return;
        }
        String userEmail=getEmail.getText().toString().trim();
        String userPass=getPassword.getText().toString();


        DbOperation operation=new DbOperation(getApplicationContext());
        HelperClass data=operation.userLogIN(userEmail,userPass);



        if (data !=null){
                Intent intent=new Intent(getApplicationContext(),UserView.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("name",data.getName());
                intent.putExtra("phone",data.getPhone());
                intent.putExtra("email",data.getEmail());
                intent.putExtra("password",data.getPassword());
                intent.putExtra("message","Login Successfully ! ");
                intent.putExtra("result",true);
                startActivity(intent);
            Toast.makeText(this, "Log in success", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Email or password are incorrect", Toast.LENGTH_SHORT).show();
        }
    }
}

